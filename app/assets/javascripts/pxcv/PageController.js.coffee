
# Controller for loading, showing and hiding pages.
class pxcv.PageController
  
  defaults:
    pagesPackage: 'frontend.views.pages'
    pagesContainer: '#main'
    defaultPath: 'home'
  
  _.extend @prototype, Backbone.Events
  
  constructor: (@options = {})->
    _.defaults @options, @defaults
    
    throw new Error('Must include router in options') unless @options.router?
    throw new Error('Must include loader in options') unless @options.loader?
    
    @loader = @options.loader
    @router = @options.router
    
    @listenTo @router, 'route', @routeToPage

  # Call matching method on this controller when routing (if it exists).
  routeToPage: (route, params) ->
    @[route](params...) if _.isFunction @[route]
  
  changePage: (klass, options) ->
    # @currentPage will be null on first load
    if @currentPage?
      @loadPage(klass, options)
    else
      # use existing markup on first load
      @currentPage = new frontend.views.pages[klass] _.extend {}, options,
        el: $(@options.pagesContainer).children().first()
      @currentPage.show()
    
  loadPage: (klass, options) ->
    # TODO still some issues with this, can break when very quickly switching pages
    
    if @loading?.state() is 'pending'
      # abort AJAX load if in progress
      @loading.abort()
    else if @newPage?.hidden
      # remove previous new page if it never showed
      @newPage.remove()
    
    oldPage = @currentPage
    @newPage = new frontend.views.pages[klass](options)
    # TODO probably can be cleaned up using $.when instead of then
    # see app/assets/javascripts/frontend/views/pages/Default.js.coffee
    
    # hide old page concurrently with new page load
    hiding = oldPage.hide().done =>
      # only show loader if load is still in progress
      @loader.show() if @loading.state() is 'pending'
    
    # load new page
    @loading = @newPage.load(pxcv.currentPath(default: @options.defaultPath))
    @loading.then =>
        # promise from old page hide as a return value
        hiding
      # loader may never be shown and hide will immediately resolve
      .then(@loader.hide)
      # done only called after old page and loader are hidden
      .then =>
        @newPage.show()
    
    @currentPage = @newPage
  