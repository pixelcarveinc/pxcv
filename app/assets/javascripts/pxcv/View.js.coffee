
# Extend Backbone.View with animation and resizing capabilities.
class pxcv.View extends Backbone.View
  
  animationDefaults:
    animate: true
    time: 0.5
    delay: 0 
  
  # Whether to render view when window is resized.
  renderOnResize: true

  constructor: (options = {}) ->
    # setup hide/show state and a resolved deferred object for state
    if options.startShown
      @shown = true
      @showDeferred = $.Deferred().resolve()
    else
      @shown = false
      @hideDeferred = $.Deferred().resolve()
      
    super

  # Animate view shown.  Triggers shown event when complete, returns a jQuery
  # promise object for more complex event handling.
  #
  # options:
  # animate     Whether to animate state change, default true.
  show: (options = {}) =>
    # already shown, return existing promise
    return @showDeferred.promise() if @shown
    
    _.defaults options, @animationDefaults
    
    # hide animation is not resolved, will be canceled by this one
    @hideDeferred.reject() if @hideDeferred.state() is 'pending'
    
    @hidden = not @shown = true
    @showDeferred = $.Deferred()
    
    $(window).on('resize', @_resize) if @renderOnResize
    @render()
    
    # set hidden after render to make sure elements are available as needed
    @$el.css('visibility', 'hidden')
      
    @showAnimation(options)
    @showDeferred.promise()
      
  # Performs view show animation(s).  Overridable, but make sure to call
  # _shown after animations complete.
  showAnimation: (options) ->
    if options.animate
      TweenLite.to @$el, options.time,
        delay: options.delay
        autoAlpha: 1
        ease: Sine.easeOut
        onComplete: @_setShown
    else
      @$el.css('visibility', 'visible')
      @_setShown()
    
  # Animate view hidden.  Triggers hidden event when complete, returns a jQuery
  # promise object for more complex event handling.
  #
  # options:
  # animate     Whether to animate state change, default true.
  hide: (options = {}) =>
    # already hidden, return existing promise
    return @hideDeferred.promise() if not @shown
    
    _.defaults options, @animationDefaults
    
    # hide animation is not resolved, will be canceled by this one
    @showDeferred.reject() if @showDeferred.state() is 'pending'
    
    @hidden = not @shown = false
    @hideDeferred = $.Deferred()
    
    @hideAnimation(options)
    @hideDeferred.promise()
  
  # Performs view hide animation(s).  Overridable, but make sure to call
  # _hidden after animations complete.
  hideAnimation: (options) ->
    if options.animate 
      TweenLite.to @el, options.time,
        delay: options.delay
        autoAlpha: 0
        ease: Sine.easeOut
        onComplete: @_setHidden
    else
      @$el.css('visibility', 'hidden')
      @_setHidden()
  
  remove: ->
    $(window).off('resize', @_resize)
    super

  # Wrapper to keep this context and allow for event removal by function.
  _resize: => @render()
  
  _setShown: =>
    @showDeferred.resolve()
    @trigger 'shown', @
  
  _setHidden: =>
    @hideDeferred.resolve()
    @trigger 'hidden', @
