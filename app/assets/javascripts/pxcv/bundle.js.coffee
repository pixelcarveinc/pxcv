# Require core components of pxcv-rails library.
#
# NOTE: THIS FILE DOES NOT AUTOMATICALLY REQUIRE ALL CLASSES IN THE LIBRARY!
#
# This file will only include the bare minimum needed to use the library
# including the pxcv namespace, core methods, JavaScript/jQuery/etc extensions,
# JST/HAML helpers and templates.
#
# In order to use the other parts of this library you MUST require the files
# either individually where needed or by using a require_tree statement in a
# manifest file.  This is done purposefully to prevent the entire library from
# being included in your application and creating bloat.
#
#= require pxcv/namespace
#= require_tree ./ext
#= require_tree ./core
#= require_tree ./helpers
#= require_tree ../../templates
#= require jquery.cookie
