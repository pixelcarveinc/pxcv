# Core functions in pxcv namespace.


# Get current path from history with options for start/end points and default
# value.
#
# Options:
# default     - Default path to return if there is no fragment in history.
# from        - Point within path to start returning from.
# to          - Maximum segment length to return.
#
# @params {object} Optional hash of options.
# @returns {string}
pxcv.currentPath = (options = {}) ->
  _.defaults options,
    default: 'home'
    from: 0
  
  fragment = Backbone.history.fragment || location.pathname
  
  matches = fragment.match(/[^\/]+/g)
  matches = [options.default] unless matches?
  
  matches.slice(options.from, options.to).join('/')


# Add helpers to HAML/JST/pxcv.helpers namespaces.
#
# @param {object}
# @returns {undefined}
pxcv.addHelpers = (helpers) ->
  # merge with HAML
  if HAML?
    helpers = _.extend helpers, HAML.globals()
    HAML.globals = -> helpers
  
  # merge with JST
  window.JST ||= {}
  _.extend JST, helpers
  
  # merge with pxcv.helpers
  pxcv.helpers ||= {}
  _.extend pxcv.helpers, helpers


# Similar to ActiveSupport's blank? method.  Returns true if value is
# undefined, has no length or contains only whitespace.
#
# @param {string}
# @returns {boolean}
pxcv.isBlank = (str) -> not str? or /^\s*$/.test(str)


# Build a class from a string that may include namespace.
#
# ie. my.namespace.ClassToBuild will instantiate
#
# @param {string} Name of class to build.
# @param {string} Optional default class to build if class not found.
# @returns {object|undefined}
pxcv.buildClass = (klass, def) ->
  args  = _.toArray(arguments).slice(2)
  paths = klass?.split('.')
  
  if paths?
    klass = window
    klass = klass[paths.shift()] while (klass and paths.length > 0)
  
  if klass? and paths.length is 0
    new klass args...
  else if def?
    new def args...
