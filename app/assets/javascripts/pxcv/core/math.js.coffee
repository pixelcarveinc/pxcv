# math functions

  
# Linear interpolation between two points.
pxcv.math.lerp = (p0x, p0y, p1x, p1y, t) ->
  unless p1y?
    t = p1x
    if p0x.x?
      # first two arguments are objects with x/y properties
      [p1x, p1y] = [p0y.x, p0y.y]
      [p0x, p0y] = [p0x.x, p0x.y]
    else
      # first two arguments are arrays
      [p1x, p1y] = [p0y[0], p0y[1]]
      [p0x, p0y] = [p0x[0], p0x[1]]
    
  x: p0x + (p1x - p0x) * t
  y: p0y + (p1y - p0y) * t


# Clamp value between min and max.
pxcv.math.clamp = (value, min, max) ->
  value = value + 0
  if value > max
    max
  else if value < min
    min
  else
    value
  
  
# Round value to nearest number.
pxcv.math.roundToNearest = (value, n) -> Math.round(value / n) * n

