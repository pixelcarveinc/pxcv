# CoffeeScript way of defining getter/setters.
# https://gist.github.com/alexaivars/1599437
unless Function::define?
  Function::define = (prop, desc) ->
    Object.defineProperty this.prototype, prop, desc