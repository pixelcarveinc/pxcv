# createId method on String prototype


# Convert any string into a web friendly "ID" version that does not contain
# spaces, puncutation, capitals, or non-ascii chracters.
unless String.prototype.createId?
  # new scope for create-once private variables in method
  do ->
    
    # Convert non-English alphabet latin characters to similar English characters
    latinToEnglish = {
      'À': 'A',    'Á': 'A',     'Â': 'A',     'Ã': 'A',
      'Ä': 'AE',   'Å': 'A',     'Æ': 'AE',    'Ç': 'C',
      'È': 'E',    'É': 'E',     'Ê': 'E',     'Ë': 'E',
      'Ì': 'I',    'Í': 'I',     'Î': 'I',     'Ï': 'I',
      'Ð': 'D',    'Ñ': 'N',     'Ò': 'O',     'Ó': 'O',
      'Ô': 'O',    'Õ': 'O',     'Ö': 'OE',    'Ø': 'O',
      'Ù': 'U',    'Ú': 'U',     'Û': 'U',     'Ü': 'UE',
      'Ý': 'Y',    'Þ': 'Th',    'ß': 'ss',    'à': 'a',
      'á': 'a',    'â': 'a',     'ã': 'a',     'ä': 'ae',
      'å': 'a',    'æ': 'ae',    'ç': 'c',     'è': 'e',
      'é': 'e',    'ê': 'e',     'ë': 'e',     'ì': 'i',
      'í': 'i',    'î': 'i',     'ï': 'i',     'ð': 'd',
      'ñ': 'n',    'ò': 'o',     'ó': 'o',     'ô': 'o',
      'õ': 'o',    'ö': 'oe',    'ø': 'o',     'ù': 'u',
      'ú': 'u',    'û': 'u',     'ü': 'ue',    'ý': 'y',
      'þ': 'th',   'ÿ': 'y',     'Œ': 'Oe',    'œ': 'oe',
      'Š': 'S',    'š': 's',     'Ÿ': 'Y'
    }
    
    shortForms =
      '%' : '-percent-'
      '$' : '-dollars-'
      '=' : '-equals-'
      '°' : '-degrees-'
      '©' : '-copyright-'
      '@' : '-at-'
      '+' : '-and-'
      '&' : '-and-'
      '–' : '-'
      '—' : '-'
      '.' : '-'
      ' ' : '-'
      
    # Translate characters in a string using a translation table
    strtr = (str, table) ->
      for k, v of table
        r = RegExp k.escapeRegExp(), 'g'
        str = str.replace r, v 
      str

    String.prototype.createId = (limit) ->
          
      str = String @
  
      # lower case and convert special latin characters to ascii equivalents
      id = strtr str.toLowerCase(), latinToEnglish
  
      # replace some common short forms and spaces for dashes
      id = strtr id, shortForms
  
      # remove anything not alphabetic, numeric, dash or underscore
      id = id.replace /[^a-z0-9\_\-]/g, ''
  
      # strip out extra dashes
      id = id.replace /-{2,}/g, '-'
  
      # remove leading/trailing dashes/underscores
      id = id.replace /^[-_]+|[-_]+$/g, ''
      
      limit = parseInt limit
      unless isNaN limit
        parts = id.split '-'
        id = ''
        for part in parts
          break if (id + part).length > limit
          id += part + '-'
        id = parts[0] if id is '' and parts.length
        id = id[0..-2] if id[id.length - 1] is '-'
      
      id
    
