# Escaping extensions for String prototype


# Escape a string for usage in a RegExp
# http://stackoverflow.com/a/6969486
unless String.prototype.escapeRegExp?
  # new scope for create-once private variables in method
  do ->
    # Referring to the table here:
    # https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/regexp
    # these characters should be escaped
    # \ ^ $ * + ? . ( ) | { } [ ]
    # These characters only have special meaning inside of brackets
    # they do not need to be escaped, but they MAY be escaped
    # without any adverse effects (to the best of my knowledge and casual testing)
    # : ! , = 
    # my test "~!@#$%^&*(){}[]`/=?+\|-_;:'\",<.>".match(/[\#]/g)
    specials = [
      # order matters for these
        "-"
      , "["
      , "]"
      # order doesn't matter for any of these
      , "/"
      , "{"
      , "}"
      , "("
      , ")"
      , "*"
      , "+"
      , "?"
      , "."
      , "\\"
      , "^"
      , "$"
      , "|"
    ]
    
    # I choose to escape every character with '\'
    # even though only some strictly require it when inside of []
    regex = RegExp '[' + specials.join('\\') + ']', 'g'
    
    String.prototype.escapeRegExp = ->
      String(@).replace regex, "\\$&"