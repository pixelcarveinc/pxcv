# Miscellaneous String extensions for formatting


# Upper case first character in string.
unless String.prototype.ucfirst?
  String.prototype.ucfirst = ->
    str = String @
    return str unless str.length
    str[0].toUpperCase() + str[1..-1]


# Lower case first character in string.
unless String.prototype.lcfirst?
  String.prototype.lcfirst = ->
    str = String @
    return str unless str.length
    str[0].toLowerCase() + str[1..-1]
    

###
# TODO tests
#
# "created_at".camelize()                # => "CreatedAt"
# "created_at".camelize(false)           # => "createdAt"
# "active_model/errors".camelize()       # => "ActiveModel.Errors"
# "active_model/errors".camelize(false)  # => "activeModel.Errors"
###

# camelize method similar to Rails'.
#
# By default, camelize converts strings to UpperCamelCase. If the argument to
# camelize is set to false then camelize produces lowerCamelCase.
#
# camelize will also convert '/' to '.' which is useful for converting paths to
# namespaces.
unless String.prototype.camelize?
  String.prototype.camelize = (upperFirst=true) ->
    # TODO very quick and dirty conversion from Rails, add tests
    str = String @
    str = str.ucfirst() if upperFirst
    str = str.replace /(?:_|(\/)|-)([a-z\d]*)/, (match, p1, p2) ->
      p1 + p2.ucfirst()
    str = str.replace '/', '.'


# TODO tests
#
# "employee_salary".humanize()    # => "Employee salary"
# "author_id".humanize()          # => "Author"

# humanize method similar to Rails'.
#
# Capitalizes the first word and turns underscores into spaces and strips a
# trailing “_id”, if any. Like titleize, this is meant for creating pretty
# output.
unless String.prototype.humanize?
  String.prototype.humanize = ->
    # TODO very quick and dirty conversion from Rails, add tests
    str = String @
    str = str.replace /_id$/, ''
    str = str.replace /_/, ' '
    str = str.replace /^\w/, (match) -> match.toUpperCase()
    

# underscore method similar to Rails'.
unless String.prototype.underscore?
  String.prototype.underscore = ->
    str = String @
    str = str.replace '::', '/'
    str = str.replace /([A-Z\d]+)([A-Z][a-z])/, (match, p1, p2) -> p1 + '_' + p2
    str = str.replace /([a-z\d])([A-Z])/, (match, p1, p2) -> p1 + '_' + p2
    str.toLowerCase()
