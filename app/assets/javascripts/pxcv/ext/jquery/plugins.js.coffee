# jQuery plugins

$ = jQuery


# Is the scrollbar visible?
unless $.fn.hasScrollbar?
  $.fn.hasScrollbar = -> @.get(0).scrollHeight > @.get(0).clientHeight


# Get a css property as a number by stripping units and casting
# will return undefined for all cases where a numeric value cannot be obtained
# from the css property.
unless $.fn.cssNumber?
  $.fn.cssNumber = (propertyName) ->
    # can't get value from empty list or undefined property name
    return undefined if !@length or !propertyName
    # pass through value if it's undefined or already a number
    value = @css propertyName
    type = $.type(value)
    return value if type is 'undefined' or type is 'number'
    # can't get valid number from a non-numeric property
    value = value.replace(/[^0-9\-\.]/g, '')
    return undefined if !value.length
    # value is numeric string, cast as number and return
    Number(value)


# Little hack to get file select dialog to open dynamically
# http://stackoverflow.com/a/6888810
#
# Note this won't work in IE 9, file selection must be triggered by file input
# click event.
unless $.fn.filePicker
  $.fn.filePicker = -> @show().focus().click().hide()


# Determine if jQuery elements are in DOM
# http://jsperf.com/jquery-element-in-dom/2
unless $.fn.isInDOM
  $.fn.isInDOM = -> @closest('html').length > 0
  
  
# Get value of Rails CSRF token from meta.
unless $.csrfToken
  $.extend
    csrfToken: -> $('head meta[name=csrf-token]').attr('content')
    
# Like jQuery's built-in offset method but accounts for padding, margin and borders.
unless $.fn.outerOffset
  $.fn.outerOffset = ->
    offset = @.offset()
      
    # assume content-box if undefined (ie. a browser that doesn't support the property)
    isBorderBox = @.css('box-sizing') is 'border-box'
    
    offset.top  -= parseInt @.css('margin-top')
    offset.top  -= parseInt @.css('padding-top')
    offset.top  -= parseInt @.css('border-top-width') unless isBorderBox
    
    offset.left -= parseInt @.css('margin-left')
    offset.left -= parseInt @.css('padding-top')
    offset.left -= parseInt @.css('border-top-width') unless isBorderBox
    
    offset
