
# helper methods for making TinyMCE a little less painful

if tinymce?
  
  # tinymce namespace
  _.extend tinymce,
    # safely create/destroy TinyMCE editor
    toggle: (id, enabled) ->
      inst = @getInstanceById id
      if enabled
        unless inst?
          @execCommand 'mceAddControl', false, id
      else
        if inst?
          @execCommand 'mceRemoveControl', false, id
          
      # fix MS Word content
      pastePreProcess = (pl, o) ->
        return if !o.wordContent
        str = o.content
        str = str.replace(/\r?\n/g, ' ')
        str = str.replace(/<\/(?:p|div|h\d|address|article|li|pre|section|tr)>/g, "\0\n")
        str = $('<div>' + str + '</div>').text()
        
        lines = for line in str.split(/\n/) when line.search(/^\s*$/) is -1
          '<p>' + line + '</p>'

  o.content = lines.join "\n"
          
          
  # Editor class
  _.extend tinymce.Editor.prototype,
    # convienience getters for iframe and table width/height
    iframeHeight: -> $("##{@id}_ifr").height()
    iframeWidth:  -> $("##{@id}_ifr").width()
    tableHeight:  -> $("##{@id}_tbl").height()
    tableWidth:   -> $("##{@id}_tbl").width()
