

_baseUrl = '/'

pxcv.addHelpers
  
  # Get baseUrl, optionally prepending to passed url.
  # 
  # @param {String} url
  # @returns {String}
  baseUrl: (url) ->
    if url?
      url = String url
      
      # must not start with a slash
      url = url.slice(1) if url[0] is '/'
      
      _baseUrl + url
    else
      _baseUrl
   
  # Set the value for baseUrl.
  # 
  # @param {String} value
  # @returns {String}
  setBaseUrl: (value) ->
    _baseUrl = String value
    
    # must always end with a slash
    _baseUrl += '/' if _baseUrl[_baseUrl.length - 1] isnt '/'

    _baseUrl