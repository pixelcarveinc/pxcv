
pxcv.addHelpers

  # Proxy can calls to provided abilities
  can: (action, subject) =>
    @abilities ||= []
    can = _.find @abilities, (a) -> a.can action, subject
    can?
    
  # Proxy cannot calls to provided abilities
  cannot: (action, subject) =>
    !@can action, subject
    
  # Add an ability class for can/cannot tests
  addAbility: (ability) =>
    @abilities ||= []
    @abilities.push ability
    
  # Remove an ability class
  removeAbility: (ability) =>
    return unless ability? and @abilities?
    i = @abilities.indexOf ability
    @abilities.splice i, 1 if i > -1
