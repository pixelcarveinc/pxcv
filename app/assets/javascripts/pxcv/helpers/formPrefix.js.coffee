

pxcv.addHelpers
    
  # Add prefix to field name.
  # 
  # @param {String} name
  # @param {String | undefined} prefix
  # @returns {String}
  formPrefix: (name, prefix) ->
    if prefix?
      a = if _.isArray(prefix) then prefix else [prefix]
      a.push name
      
      name = a[0] + _.map(a.slice(1), (s) -> "[#{s}]").join('')
      
    name