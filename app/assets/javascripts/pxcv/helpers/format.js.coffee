#= require date.format


pxcv.addHelpers

  # format date to time, month or year depending on how long from date
  adaptiveDateFormat: (date, since) ->
    throw new SyntaxError('invalid date') unless date?
    # allow passing string or date object
    date  = new Date date unless date.setMinutes?
    since = new Date unless since
    since = new Date since unless since.setMinutes?
    
    if since - date < 24 * 60 * 60 * 1000 and since.getDay() is date.getDay()
      format = 'h:MM tt'
    else if date.getFullYear() is since.getFullYear()
      format = 'mmm d'
    else
      format = 'yyyy'
        
    date.format format
  
  
  # convert file size to a human readable string
  # rewritten from http://stackoverflow.com/a/14919494
  numberToHumanSize: (bytes, si) ->
    return unless bytes?
    
    thresh = if si then 1000 else 1024
    return bytes.toString() if bytes < thresh
    units = if si then ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'] else ['KB','MB','GB','TB','PB','EB','ZB','YB']
    u = -1
    while bytes >= thresh
      bytes /= thresh
      ++u
    bytes.toFixed(0) + ' ' + units[u]
