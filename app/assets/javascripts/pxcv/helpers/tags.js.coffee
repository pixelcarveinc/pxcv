# TODO what about escaping in attributes?


# HTML Tag class
#
# Programmatically generate HTML so you aren't scotch-taping strings together all the time.
# Usage: targetString = Tag.make(tagName, attributeHash, text)
# The text is NOT escaped by default. Use the special attribute 'escapeText': true to escape the inner text.
class Tag
  constructor: (@tagName, @attributes = {}, @text = "") ->
    
    if @attributes.checked is true
      @attributes.checked = 'checked'
    else
      delete @attributes.checked
      
    if @attributes.disabled is true
      @attributes.disabled = 'disabled'
    else
      delete @attributes.disabled
      
    if @attributes['escapeText']
      delete @attributes['escapeText']
      @text = HAML.escape(@text)

  # Create a tag as string
  @make: (tagName, attributes, text) ->
    t = new Tag(tagName, attributes, text)
    t.toString()

  openTag: ->
    close = if @text then '>' else ' />'
    attributeStrings = []
    attributeStrings.push("#{HAML.escape(key)}=\"#{HAML.escape(attribute)}\"") for key, attribute of @attributes
    attributeString = if attributeStrings.length > 0 then ' ' + attributeStrings.join(' ') else ''

    "<#{@tagName}#{attributeString}#{close}"

  closeTag: (tagName) ->
    "</#{@tagName}>"

  toString: ->
    s = ''
    s += @openTag()
    s += @text
    s += @closeTag() if @text
    s



pxcv.addHelpers

  # general

  linkTo: (name, href, attrs = {}) ->
    attrs['href'] ||= href
    Tag.make 'a', attrs, name
  
  # Attempts to get source from assets if it's not an absolute path
  imageTag: (source, options={}) ->
    options.src = source ? ''
    unless options.src.match(/^(?:cid|data|\/):/) or options.src.length is 0
      path = @imagePath options.src
      options.src = path if path?
    
    if options.size
      size = options.size
      delete options.size
      [options['width'], options['height']] = size.split 'x' if size.match /^\d+x\d+$/
      
    Tag.make 'img', options
    
  facebookLike: (url, options={}) ->
    attrs = {}
    attrs['class']     = 'fb-like'
    attrs['data-href'] = url
    
    for k, v of options
      attrs['data-' + k] = v
    
    Tag.make 'div', attrs


  # form

  textField: (name, value = "", attrs = {}) ->
    attrs['type']  = 'text'
    attrs['name']  = name
    attrs['value'] = value
    Tag.make 'input', attrs

  emailField: (name, value = "", attrs = {}) ->
    attrs['type']  = 'email'
    attrs['name']  = name
    attrs['value'] = value
    Tag.make 'input', attrs

  passwordField: (name, value = "", attrs = {}) ->
    attrs['type']  = 'password'
    attrs['name']  = name
    attrs['value'] = value
    Tag.make 'input', attrs

  fileField: (name, attrs = {}) ->
    attrs['name'] = name
    attrs['type'] = 'file'
    
    if attrs.multiple is true
      attrs.multiple = 'multiple'
    else
      delete attrs.multiple
      
    Tag.make 'input', attrs

  textarea: (name, value = "", attrs = {}) ->
    attrs['name'] = name
    Tag.make 'textarea', attrs, value

  hidden: (name, value = "", attrs = {}) ->
    attrs['type']  = 'hidden'
    attrs['name']  = name
    attrs['value'] = value
    Tag.make 'input', attrs

  # Create a select tag with options
  # @param collection is used to create the options. Can be an Array of Strings (used as text and value), or Array of [text, value]
  # @params attrs can include the options:
  #   * selected: value of the option that should be selected
  #   * caption: Text to use as first option (blank with blank value)
  #   * any other option will be used as HTML attribute in the select tag
  # Examples:
  #   select('country', ['US', 'ES', 'GB', 'SE'], {id: 'country-selector', selected: 'ES'})
  #   select('language', [['English', 'en'], ['Spanish', 'es']], {caption: 'Select Language'})
  select: (name, collection = [], attrs = {}) ->
    attrs['name'] = name
    selectBody = []
    selectBody.push Tag.make('option', {value: ''}, attrs['caption']) if attrs['caption']
    for opt in collection
      if opt instanceof Array
        body = opt[0]
        value = opt[1]
      else
        body  = opt
        value = opt
      selectOptions = {}
      selectOptions['value'] = value
      selectOptions['selected'] = 'selected' if attrs['selected'] && attrs['selected'].toString() == value.toString()
      selectOptions['escapeText'] = true
      selectBody.push Tag.make('option', selectOptions, body)

    delete attrs['selected']
    delete attrs['caption']
    Tag.make 'select', attrs, selectBody.join('')

  checkBox: (name, value = 1, attrs = {}) ->
    attrs['type']    = 'checkbox'
    attrs['name']    = name
    attrs['value']   = value
    
    Tag.make 'input', attrs, null

  label: (name, content = "", attrs = {}) ->
    attrs['for'] = name
    Tag.make 'label', attrs, content

  submit: (value = "", attrs = {}) ->
    attrs['type']   = 'submit'
    attrs['value']  = value
    Tag.make 'input', attrs
