

prefixes = {}

pxcv.addHelpers
  
  # Generate a unique ID with prefix.
  # 
  # @param {String} prefix
  # @returns {String}
  uniqueId: (prefix) ->
    prefix = String prefix
    prefixes[prefix] ||= []
    
    id = prefix + '-' + String(prefixes[prefix].length + 1)
    prefixes[prefix].push id
    
    id
