

# Singleton for enabling/disabling mouse cursor via CSS for display elements
# that cannot change the mouse cursor (ie. canvas elements).
class MouseCursorManager
  
  constructor: ->
    @_enabled    = 0
    @_cssEnabled = false
  
  
  listenTo: (target) ->
    throw new TypeError('Invalid target') unless target? and target.on?
    target.on 'mouseover.mcm', @_enable
    target.on 'mouseout.mcm dragend.mcm',  @_disable
    
  stopListening: (target) ->
    throw new TypeError('Invalid target') unless target? and target.on?
    target.off 'mouseover.mcm mouseout.mcm dragend.mcm'
    
  
  _enable: (e) =>
    @_enabled += 1

    if not @_cssEnabled and @_enabled > 0 
      @_cssEnabled = true
      document.body.style.cursor = 'pointer'
      
  _disable: (e) =>
    @_enabled -= 1
    @_enabled = 0 if @_enabled < 0
    
    if @_cssEnabled and @_enabled is 0
      @_cssEnabled = false
      document.body.style.cursor = 'auto'
    
  
instance = new MouseCursorManager

Object.defineProperty pxcv.kinetic, 'mouseCursorManager',
  get: -> instance