

# Simple matrix class modeled after AS3's version.
class window.pxcv.math.Matrix
  
  constructor: (@a=1, @b=0, @c=0, @d=1, @tx=0, @ty=0) ->

  identity: ->
    @a = @d = 1
    @b = @c = @tx = @ty = 0
    @

  clone: ->
    new Matrix @a, @b, @c, @d, @tx, @ty


  # TODO add rest of AS3 methods as needed