#= require pxcv/math/Matrix

Matrix = pxcv.math.Matrix


# Scale functions for standalone use or as a mixin.
pxcv.math.Scale =
  
  # Calculate a matrix that will provide translations and scaling to crop an
  # object of original width/height to new width/height.
  #
  # Aliased to cover.
  crop: (orgWidth, orgHeight, newWidth, newHeight) ->
    matrix = new Matrix

    matrix.a = newWidth  / orgWidth
    matrix.d = newHeight / orgHeight
    

    # wider, scale X, crop Y
    if matrix.a > matrix.d
      # calculate difference between source height and target height
      # to find offset
      matrix.ty = -(orgHeight * matrix.a - newHeight) / 2

    # longer, crop X
    else if matrix.d > matrix.a
      # calculate difference between source width and target width to
      # find offset
      matrix.tx = -(orgWidth * matrix.d - newWidth) / 2

    # always stay in ratio
    if matrix.d < matrix.a
        matrix.d = matrix.a
    else
        matrix.a = matrix.d

    matrix
    
    
  # Calculate a matrix that will provide translations and scaling to letterbox
  # an object of original width/height to fit within new width/height.
  #
  # Aliased to contain.
  letterbox: (orgWidth, orgHeight, newWidth, newHeight) ->
    matrix = new Matrix

    oa = orgWidth / orgHeight
    na = newWidth / newHeight
    
    if na > oa
      # taller
      matrix.a = matrix.d = newHeight / orgHeight
      # calculate difference between source width and target width to
      # find offset
      matrix.tx = -(orgWidth * matrix.d - newWidth) / 2
    else
      # wider
      matrix.a = matrix.d = newWidth / orgWidth
      # calculate difference between source height and target height to
      # find offset
      matrix.ty = -(orgHeight * matrix.d - newHeight) / 2

    matrix
    
    
  # Alias for crop.
  cover: -> @crop arguments...
  
  # Alias for letterbox.
  contain: -> @letterbox arguments...