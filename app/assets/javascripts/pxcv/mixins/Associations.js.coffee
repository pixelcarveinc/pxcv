#= require pxcv/mixins/Mixin


# Mixin giving models ability to set/save associated 1:1 and 1:many models.
# 
# Associations may be specified on protype as associations key and at mixin time
# they will automatically be defined.
window.pxcv.mixins.Associations = pxcv.mixins.Mixin.extend
  
  initialize: ->
    @_associations = []
    
    if @associations?
      for k, a of _.result(@, 'associations')
        @defineAssociation a.type, k, a.build, _.omit(a, 'type', 'build')
    
    # apply associations to existing attributes
    @set @attributes
    
  # Define a new association.
  # 
  # Options:
  # create   OPTIONAL handler called with newly instantiated model/collection
  # 
  # @param {String} type
  # @param {String} key
  # @param {Function} build
  # @param {Object} options
  # @returns {this}
  defineAssociation: (type, key, build, options) ->
    @_associations.push _.extend { type: type, key: key, build: build }, options
    
    # build an empty collection if not set
    @set(key, []) if type is 'collection' and not @has(key)
    
    @

  set: (key, val, options) ->
    return @ unless key?
    
    # Handle both `"key", value` and `{key: value}` -style arguments.
    if typeof key is 'object'
      attrs   = key
      options = val
    else
      (attrs = {})[key] = val
    
    # in case an object was passed for attributes, _assocSet may change attrs
    attrs = _.clone attrs
    @_assocSet attrs
    
    @constructor.__super__.set.call @, attrs, options
    @
    
  # Accepts full option which will include sub-models.
  # 
  # @param {Object} options
  # @returns {Object}
  toJSON: (options = {}) ->
    values = @constructor.__super__.toJSON.call @, options

    # optionally include some or all sub-models in toJSON output
    options.include ||= []
    
    if options.full
      # include all sub-models if full is true
      options.include = (a.key for a in @_associations) 
      
    for a in @_associations
      if _.indexOf(options.include, a.key) isnt -1 and @has(a.key)
        values[a.key] = @get(a.key).toJSON options
      else
        delete values[a.key]
        
    values
    
  _assocSet: (attrs) ->
    # _associations will be undefined during first set call in original ctor
    for assoc in (@_associations || [])
      val = attrs[assoc.key]
      
      if _.isObject(val) and _.isFunction(val.get)
        # value is a model/collection, attributes/models will be merged
        if assoc.type is 'collection'
          val = _.clone val.models
        else
          val = _.clone val.attributes
          
      if (assoc.type is 'collection' and _.isArray(val)) or (assoc.type is 'model' and _.isObject(val))
        # update if key is already set and is a model/collection
        if @has(assoc.key) and _.isFunction(@get(assoc.key).get)
          @get(assoc.key).set val
          delete attrs[assoc.key]
        else
          # instantiate model/collection
          attrs[assoc.key] = new assoc.build val
          assoc.create?.call @, attrs[assoc.key]
          
    undefined
