#= require pxcv/mixins/Mixin


# Adds a cleanUp step to render and remove calls as well as a rendered flag.
# 
# For use with Backbone.View, cleanUp can be used for garbage collection, it is
# called before render and remove. The isRendered method allows for cleaning up
# only after first being rendered so properties created during render do not
# need to be checked for existance. ie.
# 
# cleanUp: function() {
# 
#     if (this.isRendered()) {
#     
#         // this.mySubView only exists after rendering
#         this.mySubView.remove()
#         
#         // non-backbone events can be removed for proper garbage collection
#         window.off('.MyEvents');
#     }
# }
window.pxcv.mixins.CleansUp = pxcv.mixins.Mixin.extend 
  
  # Happens at mixin time (usually in constructor).
  # 
  # @returns {undefined}
  initialize: ->
    proto = @constructor.prototype
    
    @_rendered = false
    
    # create an empty implementation of cleanUp if one does not exist
    unless proto.cleanUp?
      proto.cleanUp = ->
    
    # wrap cleanUp to reset rendered flag
    proto.cleanUp = _.wrap proto.cleanUp, (cleanUp) ->
      ret = cleanUp.apply @, _.rest(arguments)
      @_rendered = false
      ret
    
    # wrap render so cleanUp calls first and rendered flag is set
    proto.render = _.wrap proto.render, (render) ->
      @cleanUp()
      ret = render.apply @, _.rest(arguments)
      @_rendered = true
      ret
      
    # wrap remove so cleanUp calls before removal
    proto.remove = _.wrap proto.remove, (remove) ->
      @cleanUp()
      remove.apply @, _.rest(arguments)
  
  # Has the view been rendered at least once?
  # 
  # @return {Boolean}
  isRendered: -> @_rendered