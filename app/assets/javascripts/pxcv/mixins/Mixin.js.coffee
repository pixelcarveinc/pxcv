#= require pxcv/utils/Dictionary


# An extended mixin class that allows for code to be run in the target instance
# during construction.
# 
# Usage:
# 
#   var MyMixin = Mixin.extend({
#   
#       // initialize method runs during construction, by default at end
#       initialize: function() {
#          this.instanceVar = true;
#       },
#       
#       // all other properties are mixed into prototype
#       myMethod: function() {
#       
#       }
#   }, {
#   
#      // class properties/functions will be mixed into class
#      myClassProp: true
#   });
#   
#   var MyClass = Backbone.View.extend({
#   
#      initialize: function() {
#          // mixins may be initialized before the end of constructor if needed
#          this.initializeMixins();
#      }
#   });
#   
#   // mixin to MyClass
#   MyClass = MyMixin.mixin(MyClass);
# 
class window.pxcv.mixins.Mixin
    
  # Mixin this class into another
  # 
  # @param {Object, Function} namespace Either class to be mix into or a
  # namespace for overwriting original definition.
  # @param {String} name If namespace is passed the name of the class to
  # be mixed into must be specified.
  # @returns {Function} The new constructor after mixin in.
  @mixin: (namespace, name) ->
    ctor = if name? then namespace[name] else namespace
    
    # has this class been mixed into at least once?
    unless classesMixins.isset(ctor)
      # need to setup class for mixins
      ctor = setupMixins ctor
      # if namespace has been passed original definition can be overwritten
      namespace[name] = ctor if name?
      
    # mixin class properties
    _.extend ctor, _.omit(@, 'prototype', 'initialize', 'mixin', 'extend', '__super__')
    # mixin protoype properties
    _.extend ctor.prototype, _.omit(@prototype, 'initialize', 'constructor')
      
    # add mixin to stack
    classesMixins.get(ctor).mixins.push @
    ctor
    
  # Backbone's extend ability.
  @extend: Backbone.Model.extend
  
  
# private

# Manages mixins for a constructor.
class Mixins
  
  # @param {Function} ctor
  # @param {Function} prevIM OPTIONAL
  # @returns {Mixins}
  constructor: (@ctor, @prevIM) ->
    @mixins = []
   
  # Create a new constructor wrapping existing one with mixin intialization.
  # 
  # @returns {Function}
  createCtor: ->
    mixins = @
    
    # new constructor calls initialize mixins at end by default
    newCtor = ->
      mixins.ctor.apply @, arguments
      @initializeMixins()
      
    # fix prototype
    newCtor.prototype = @ctor.prototype
    # copy original class properties into new
    _.extend newCtor, _.omit(@ctor, 'prototype')
    newCtor.constructor = @ctor
    
    # set initialize mixins method on prototype so it may be called anywhere in ctor
    newCtor.prototype.initializeMixins = newCtor.initializeMixins = @createIM()
    
    newCtor
    
  # Create an initialize mixins function wrapped around this object.
  # 
  # @returns {Function}
  createIM: ->
    mixins = @
    -> mixins.initialize @ 
    
  # Initialize mixins for object instance.
  # 
  # @param {Object} inst
  # @returns {undefined}
  initialize: (inst) ->
    # flag prevents function from being called more than once
    return if inst.__mixinsInitialized
    
    # descend into parent's mixin initialize method if it exists
    @prevIM?.call(inst)
    
    # run mixin intializer if it exists
    m.prototype.initialize?.call(inst) for m in @mixins
    
    inst.__mixinsInitialized = true
    undefined


# dictionary storing all mixins for class constructors
classesMixins = new pxcv.utils.Dictionary

# Create mixins object for a constructor and store.
# 
# @param {Function} ctor
# @returns {Function}
setupMixins = (ctor) ->
  # will be chained with new ctor's initializeMixins method
  prevIM = ctor.initializeMixins if ctor.initializeMixins?
  
  mixins  = new Mixins ctor, prevIM
  newCtor = mixins.createCtor()
  
  classesMixins.set newCtor, mixins
  
  newCtor



# TESTS
###
window.lib ||= {}
Mixin = pxcv.mixins.Mixin

lib.Mixin1 = Mixin.extend
  initialize: ->
    console.log 'Mixin1 init'

lib.Mixin2 = Mixin.extend
  initialize: ->
    console.log 'Mixin2 init'

lib.Mixin3 = Mixin.extend
  initialize: ->
    console.log 'Mixin3 init'

lib.Base = Backbone.View.extend
  constructor: ->
    console.log 'Base ctor'
    @initialize?.call @

lib.Mixin1.mixin lib, 'Base'

lib.MyClass1 = lib.Base.extend
  initialize: ->
    console.log 'MyClass1 init'

lib.MyClass2 = lib.Base.extend
  initialize: ->
    console.log 'MyClass2 initialize mixins before end of ctor'
    @initializeMixins()
    console.log 'MyClass2 init'

lib.Mixin2.mixin lib, 'MyClass2'
###
