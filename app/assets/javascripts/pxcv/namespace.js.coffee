
# common namespace for holding this library's classes and methods.
window.pxcv =
  kinetic:            {}
  math:               {}
  mixins:             {}
  utils:              {}
  views:
    dialogs:            {}
    forms:              {}
    utils:              {}
