#= require pxcv/views/dialogs/DefaultEase


# Override Rails UJS confirm handler and use jQuery UI dialogs
#
# http://lesseverything.com/blog/archives/2012/07/18/customizing-confirmation-dialog-in-rails/

rails = $.rails

rails.allowAction = (link) ->
  return true unless link.attr('data-confirm')
  rails.showConfirmDialog(link) if rails.fire link, 'confirm'
  false
  
rails.confirmed = (link) ->
  link
    .removeAttr('data-confirm')
    .trigger('click.rails')
    
rails.confirmDialogTemplate = JST['pxcv/dialogs/confirm']
    
rails.showConfirmDialog = (link) ->
  message  = link.attr 'data-confirm'
  method   = link.attr 'data-method'
  method ||= 'get'
  
  title = switch method
    when 'get'  then 'Show'
    when 'post', 'patch', 'put' then 'Save'
    when 'delete' then 'Delete'
  
  html = rails.confirmDialogTemplate
    title:   title
    message: message

  options =
    resizable: false
    modal:     true
    buttons:
      OK: ->
        rails.confirmed link
        $(@).dialog 'close'
        rails.fire link, 'confirm:complete', [true]
      Cancel: ->
        $(@).dialog 'close'
        rails.fire link, 'confirm:complete', [false]

  $(html).dialog _.extend(options, pxcv.views.dialogs.DefaultEase)