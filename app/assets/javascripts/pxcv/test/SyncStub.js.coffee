
# sync stubs
window.SyncStub =

  # generic succes sync with no response  
  success: (method, model, options) -> options.success.call @, {}
    
  # generic error sync
  error: (method, model, options) -> options.error.call @, {error: 'test error'}
    
  # custom success sync with response from JST template
  template: (template) ->
    if template?
      func = (method, model, options) ->
        options.success.call @, JSON.parse JST[template]()
    func ? @success