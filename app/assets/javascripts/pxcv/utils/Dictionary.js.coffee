

 # A hash like object that stores keys as their original type and uses strict
 # equality for key checks.
class window.pxcv.utils.Dictionary
  
  constructor: ->
    @keys   = []
    @values = []
    @length = 0

  # Set a value for a key.
  # 
  # @param {mixed} key
  # @param {mixed} value
  # @returns {this}
  set: (key, value) ->
    index = _.indexOf @keys, key
    
    if index is -1
      @keys.push key
      @values.push value
      @length = @keys.length
    else
      @values[index] = value
    
    @
    
  # Get a value for a key or undefined if key is not set.
  # 
  # @param {mixed} key
  # @returns {mixed or undefined}
  get: (key) -> 
    index = _.indexOf @keys, key
    if index is -1 then undefined else @values[index]

  # Unset a key.
  # 
  # @param {mixed} key
  # @returns {this}
  unset: (key) ->
    index = _.indexOf @keys, key

    if index isnt -1
      @keys.splice index, 1
      @values.splice index, 1
      @length = @keys.length
      
    @
    
  # Check if a key has been set.
  #
  # @param {mixed} key
  # @returns {Boolean}
  isset: (key) -> _.indexOf(@keys, key) isnt -1

  # Return a copy of internal keys array.
  # 
  # @returns {Array}
  getKeys: -> @keys.slice()
    
  # Return a copy of internal values array.
  # 
  # @returns {Array}
  getValues: -> @values.slice()
