

# Application facade and entry point base class
class pxcv.views.FrontBase extends Backbone.View
  
  el: 'body'
  
  events:
    'click a.navigate': 'navigate'
  
  defaults:
    baseUrl:    '/'
    standalone: true
    
    
  # Parse options and do some basic setup, main view setup will happen in start
  initialize: (options) ->
    @options = _.defaults options ? {}, @defaults
    @haml    = HAML.globals()
    
    @_setupBaseUrl()
    @_setupAbility()
    @_setupCurrentUser()
    
  # Separate start call from initialize so front may be assigned in namespace
  # before sub-views relying on it are initialized.
  #
  # Extending classes should override in order to setup routers, but must call
  # super
  start: ->
    @_standalone() if @options.standalone
        
        
  # Sanitize baseUrl option and pass along to components that require it
  # baseUrl will be in the form /path/to/base/url/
  _setupBaseUrl: ->    
    @options.baseUrl  = @options.baseUrl.replace /^\/|\/$/g, ''
    @options.baseUrl  = '/' + @options.baseUrl
    @options.baseUrl += '/' if @options.baseUrl.length > 1
    
    # one time HAML helpers setup per front
    if @options.standalone
      @haml.baseUrl = @options.baseUrl
    
  # Setup ability class for cancan
  _setupAbility: ->
    return unless @options.ability
    
    @ability = new Ability rules: @options.ability
    @haml.addAbility @ability
    
  # If a current user has been passed in options it must be passed to session
  _setupCurrentUser: ->
    # current user may be a JSON object or a Session
    if !@currentUser and @options.currentUser?.get?
      @currentUser = @options.currentUser
    
    # one time ability/session setup per front
    if @options.standalone and @currentUser
      # setup user specific ability if one exists
      @haml.addAbility @currentUser.get 'ability' if @currentUser.has 'ability'
      
      @listenTo @currentUser, 'login',   @_addUserAbility
      @listenTo @currentUser, 'destroy', @_removeUserAbility
    
    
  # Called if running in standalone mode, makes calls that should only be done
  # by one application front
  _standalone: ->
    # one time event/history setup per front
    @delegateEvents
      'click a.navigate': '_navigate'
      
    Backbone.history.start pushState: true, root: @options.baseUrl
    
  # Intercept navigate links and pass through history
  _navigate: (e) ->
    # don't route if link has already been handled, allow opening new tabs
    unless e.isDefaultPrevented() or e.altKey or e.ctrlKey or e.metaKey or e.shiftKey
      e.preventDefault()
      
      url = $(e.currentTarget).attr 'href'
      url = url.slice @options.baseUrl.length
      Backbone.history.navigate url, trigger: true
    
    
  # Ability changes from session
  
  _addUserAbility: ->
    @haml.addAbility @currentUser.get 'ability'
    
  _removeUserAbility: ->
    @haml.removeAbility @currentUser.get 'ability'
