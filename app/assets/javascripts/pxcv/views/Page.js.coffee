#= require pxcv/View

# Base class for all specific page types.
class pxcv.views.Page extends pxcv.View
  
  pageContainer: '#content'
  
  constructor: ->
    super
    # trigger loaded if in DOM
    @loaded() if @$el.closest('html').length > 0
  
  # Load HTML from path and replace view's element with result.
  load: (path) ->
    $.get "/#{path}", @_setLoaded, 'html'
    
  # Implement for initialization after DOM is loaded into view.
  loaded: ->
  
  # Handler for AJAX load call, will setup $el and add to DOM as well as
  # triggering loaded handler.
  _setLoaded: (html) =>
    @setElement html
    $(@pageContainer).append(@$el)
    @loaded()
    
  # Pages are removed when hidden.
  _setHidden: =>
    @remove()
    super
