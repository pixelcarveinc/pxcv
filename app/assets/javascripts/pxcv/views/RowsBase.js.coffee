

# Base class for views managing a series of row sub-views
class window.pxcv.views.RowsBase extends Backbone.View

  # rendering methods

  initialize: ->
    if @collection?
      @listenTo @collection, 'add',     @addRow
      @listenTo @collection, 'sync',    @sort
      @listenTo @collection, 'destroy', @removeRow

  # Render a row for each model in collection if it's set  
  render: ->
    @cleanUp()
    @collection?.each((m) => @addRow m)
    @

  # Remove sub-views
  cleanUp: ->
    row.remove() for row in @rows if @rows?
    @rows = []
    
  remove: ->
    @cleanUp()
    super
    
  # Builds a new row from model
  # must be implemented by extending class
  # if undefined is returned by this method model will be skipped
  newRow: (model) ->

  # Add and render a single row for a model
  addRow: (model) ->
    row = @newRow model
    return unless row?
    
    if @collection?
      # insert based on index within collection
      index = @collection.indexOf model
      if index < @rows.length
        @rows[index].$el.before row.render().el
      else
        @$el.append row.render().el
      @rows.splice index, 0, row
    else
      # no collection, just append
      @$el.append row.render().el
      @rows.push row
    
    @listenTo row, 'select', (model, selected) -> @trigger 'select', model, selected
    @sort()
    
  # Remove a row
  removeRow: (model) ->
    # row must be removed from DOM and internal row cache
    row = _.find @rows, (r) -> r.model is model
    if row?
      row.remove()
      @rows = _.without @rows, row
    
  
  # sorting methods
  # only used if sorter is set
  # (should be a class extending from pxcv.views.utils.RowSorterBase)
  
  sort: -> @sortBy()
    
  sortBy: (column, direction) ->
    return unless @sorter?
    # sort rows, they will be re-attached in new order
    @pullRows -> @rows.sort @sorter.getSortFunc column, direction
      
  # Detach rows, call func, then re-attach
  pullRows: (func) ->
    @$el.children().detach() if @$el
    func.apply @
    @$el.append(row.el) for row in @rows
    
    
  # selection methods
  # row class must implement select method and have selected property
    
  # select all, none
  selectAll: ->
    state = @selectState()
    bool  = state is 'some' or state is 'none'
    row.select(bool, silent: true) for row in @rows
    
  # select state of rows, can be one of the following:
  # all
  # none
  # some
  selectState: ->
    if (_.every @rows, (r) -> r.selected)
      'all'
    else if (_.every @rows, (r) -> !r.selected)
      'none'
    else
      'some'
      
  # models for selected rows
  selected: ->
    _.pluck _.select(@rows, (r) -> r.selected), 'model'
