

# default easing for all dialogs
pxcv.views.dialogs.DefaultEase = 
  show:
    effect:   'puff'
    duration: 170
    easing:   'easeOutSine'
    percent:  75
  hide:
    effect:   'puff'
    duration: 170
    easing:   'easeInSine'
    percent:  75