#= require pxcv/views/dialogs/DefaultEase

DefaultEase = pxcv.views.dialogs.DefaultEase


# Delete dialog mixin
pxcv.views.dialogs.DeleteDialog =
  deleteTemplate: JST['pxcv/dialogs/delete']
  deleteDialog: (name, model) ->
    message = @deleteTemplate name: name
    options =
      resizable: false
      width:     370
      height:    170
      modal:     true
      buttons:
        Yes: ->
          model.destroy()
          $(@).dialog 'close'
        Cancel: ->
          $(@).dialog 'close'
      close: ->
        $(@).dialog 'destroy'
    
    $(message).dialog _.extend(options, DefaultEase)