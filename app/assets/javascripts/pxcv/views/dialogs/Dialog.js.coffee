#= require pxcv/views/dialogs/DefaultEase

DefaultEase = pxcv.views.dialogs.DefaultEase


# Backbone view wrapper for jQuery UI dialogs.
#
# Specify dialog settings by defining a dialog variable or by passing dialog
# object to constructor options.
#
# See http://api.jqueryui.com/dialog/ for available jQuery UI dialog options.
#
# dialog events:
# open
# opened
# close
# closed
# focus
# drag
# dragstart
# dragstop
# resize
# resizestart
# resizestop
class window.pxcv.views.dialogs.Dialog extends Backbone.View
  
  @DIALOG_EVENTS = 'open beforeclose close focus drag dragstart dragstop resize resizestart resizestop'

  constructor: (options) ->
    super
    @open() if @dialogOptions.autoOpen
  
  initialize: (options) ->
    options ||= {}
    defaults  =
      autoOpen: true
      modal:    true
      width:    420
      height:   500
      buttons:  {}
     
    @dialogOptions = _.extend defaults, DefaultEase, @dialog, options.dialog
    # new buttons hash so prototype version isn't overwritten
    buttons = @dialogOptions.buttons
    @dialogOptions.buttons = {}
      
    # bind button methods to this
    for label, method of buttons
      if _.isFunction method
        @dialogOptions.buttons[label] = method
      else
        @dialogOptions.buttons[label] = _.bind @[method], @

    # autoOpen in dialog is always false because it's handled in constructor so
    # initialization can be completed in sub-classes before auto opening
    @$el.dialog _.extend {}, @dialogOptions, {autoOpen: false}

    # listen for, and proxy, dialog events
    for e in @constructor.DIALOG_EVENTS.split(' ')
      @$el.on 'dialog' + e, @_proxyDialogEvent
    
  # Whether the dialog is currently open.
  isOpen: -> @$el.dialog 'isOpen'
  
  # Moves the dialog to the top of the dialog stack.
  moveToTop: -> @$el.dialog 'moveToTop'
    
  # Get/set dialog option, passing no arguments returns all options.
  dialogOption: (k, v) -> @$el.dialog 'option', k, v
  
  open: (options) ->
    defaults = render: true
    options  = _.extend defaults, options
    
    @render() if options.render
    @$el.dialog 'open'
  
  close: -> @$el.dialog 'close'
    
  # Default render template, will usually need to be overridden.
  render: ->
    @$el.html @template() if @template?
    @
  
  remove: ->
    for e in @constructor.DIALOG_EVENTS.split(' ')
      @$el.off 'dialog' + e, @_proxyDialogEvent
    @$el.dialog 'destroy'
    super
  
  # re-send dialog events as backbone events with some conversion
  _proxyDialogEvent: (e) =>
    type = switch e.type
      when 'dialogfocus'
        # trigger opened on first focus
        if @_lastDialogEvent is 'open' then 'opened' else 'focus'
      when 'dialogbeforeclose' then 'close'
      when 'dialogclose' then 'closed'
      else e.type[6..-1]
      
    # track last dialog event in order to send opened at correct time
    @_lastDialogEvent = type
    @trigger type, @
    # trigger focus after opened
    @trigger 'focus' if type is 'opened'
