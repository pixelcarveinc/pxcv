#= require pxcv/views/dialogs/Dialog
#= require pxcv/views/forms/FormMixin

Dialog       = pxcv.views.dialogs.Dialog
FormMixin    = pxcv.views.forms.FormMixin


class window.pxcv.views.dialogs.FormDialog extends Dialog
  
  className: 'form-dialog'
  
  _.extend @prototype, FormMixin
    
  initialize: ->
    @dialog.buttons =
      'Save and Close': => @save()
    super
    
    # store clean attributes in case they need to be reverted (ie. validated but not saved)
    @_cleanAttributes = _.clone @model.attributes
    
    @listenTo @model, 'invalid', @invalid
    @listenTo @model, 'error',   @error
    @listenTo @model, 'sync',    @saved
    @listenTo @,      'closed',  @remove
 
  saved: ->
    # update clean attributes on sucessful save
    @_cleanAttributes = _.clone @model.attributes
    @collection?.add @model
    @close()

  remove: ->
    # revert model state in case it was validated but not saved
    @model.set @_cleanAttributes
    super