#= require pxcv/views/dialogs/DefaultEase

DefaultEase = pxcv.views.dialogs.DefaultEase


# invalid dialog mixin
pxcv.views.dialogs.InvalidDialog =
  invalidTemplate: JST['pxcv/dialogs/invalid']
  invalidDialog: (errors) ->
    message = @invalidTemplate(errors: errors)
    options =
      resizable: false
      width:     370
      height:    260
      modal:     true
      buttons:
        Ok: ->
          $(@).dialog 'close'
      close: ->
        $(@).dialog 'destroy'
        
    $(message).dialog _.extend(options, DefaultEase)
