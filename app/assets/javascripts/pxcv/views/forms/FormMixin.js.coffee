#= require pxcv/views/dialogs/InvalidDialog

InvalidDialog = pxcv.views.dialogs.InvalidDialog


# Validate and save form to a model.
FormMixin = pxcv.views.forms.FormMixin =
  
  # Set model values from form and sync.
  save: (e) ->
    e?.preventDefault()
    @model.save @$('form').serializeHash()
  
  # JavaScript model validations failed.
  invalid: (model, errors) -> @invalidDialog errors
    
  # Errors during save could be server side validation.
  error: (model, e) ->
    errors = {}
    for field, error of e.responseJSON
      errors[field] = field.humanize() + ' ' + error
      
    # TODO if errors is empty show a generic save failed message
    @invalid model, errors 
    
  # Model saved successfully.
  saved: ->
    @collection?.add @model


_.extend FormMixin, InvalidDialog
