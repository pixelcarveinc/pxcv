#= require URI


pxcv.views.forms.UrlFinderMixin =

  # Add keydown listener for target field to this method, ideally with debounce.
  # ie.
  #
  # # delays call until 500 ms after last input
  # @prototype.findUrls = _.debounce @prototype.findUrls, 500
  #
  # Will trigger addurl and removeurl events.
  findUrls: ->
    @_foundUrls ||= []
    newUrls       = []
    
    # handle TinyMCE instances
    if @id?
      ed  = tinymce?.getInstanceById @id
      val = ed?.getContent()
      
    val = @$el.val() unless val?
    URI.withinString val, (url) => newUrls.push url
    
    same    = _.intersection @_foundUrls,   newUrls
    removed = _.difference   @_foundUrls,   newUrls
    added   = _.difference   newUrls, @_foundUrls
    
    @_foundUrls = same.concat added
    @trigger 'addurl',    url for url in added
    @trigger 'removeurl', url for url in removed
