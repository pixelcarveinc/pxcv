#= require jquery.fileupload
#= require jquery.iframe-transport


# Wrapper view for jQuery File Upload.
#
# Sends upload events when files have been selected for upload.
#
# events:
# [all callback option jQuery file upload events]
# upload          - triggered once for each added file
class window.pxcv.views.utils.FileUpload extends Backbone.View
  
  BIND: 'add submit send done fail always progress progressall start stop change paste drop dragover chunksend chunkdone chunkfail chunkalways'
  
  defaults:
    sequentialUploads:  true
    dataType:           'json'
    formAcceptCharset:  'utf-8'
    # for compatibility with ui version
    autoUpload:         true
    uploadTemplateId:   false
    downloadTemplateId: false
      
  
  initialize: (options) ->
    options ||= {}
    @uploadOptions = _.extend {}, @defaults, options.upload
    @render()
    
  # bind the AJAX based uploader to form
  render: ->
    @bind()
    @
    
  # unbind and clean up the AJAX based uploader
  remove: ->
    @unbind()
    # NOTE doesn't call super on purpose!
    
  bind: ->
    @unbind()
    
    
    # fix iframe transport CSRF issues
    if @uploadOptions.formData?
      formData = _.clone @uploadOptions.formData
    else
      formData = []
        
    formData.push
      name:  'authenticity_token'
      value: $('meta[name="csrf-token"]').attr('content')
    formData.push
      name:  'utf8'
      value: '✓'
    
    # initialize the plugin
    @$el.fileupload _.extend {}, @uploadOptions, formData: formData
    
    
    # append .json to URL in order to force a json response for iframe transport
    url = @$el.fileupload 'option', 'url'
    
    unless url?
      # get url from form
      fi = @$el.fileupload 'option', 'fileInput'
      url = $(fi.prop('form')).attr 'action'
      
    unless @uploadOptions.dataType isnt 'json' or url.match /\.json$/
      @$el.fileupload 'option', 'url', url + '.json'
    
    
    # bind events
    for event in @BIND.split(' ')
      @$el.bind "fileupload#{event}.pxcv", _.partial(@_fileUploadEvent, event)
    
    #@$el.bind 'fileuploadchange.pxcv', @addFiles
    @bound = true
    
  unbind: ->
    if @bound
      @$el.unbind '.pxcv'
      @$el.fileupload 'destroy'
      @bound = false
    
  _fileUploadEvent: (type, e) =>
    args = _.toArray(arguments).slice(1)
    # if handler exists on this, call it
    @["_#{type}"](args...) if _.isFunction @["_#{type}"]
    # re-trigger as Backbone event
    @trigger type, @, args...
    
  # triggered by fileupload when one or more files are to be uploaded
  _change: (e, data) =>
    # refresh stored jQuery reference as input may be cloned and replaced
    # https://github.com/blueimp/jQuery-File-Upload/wiki/Frequently-Asked-Questions#why-is-the-file-input-field-cloned-and-replaced-after-each-selection
    @$el = $ @$el.selector
    @trigger 'upload', @, file for file in data.files
