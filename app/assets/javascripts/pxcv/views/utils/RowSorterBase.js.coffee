#= require pxcv/views/utils/ViewSortMixin

ViewSortMixin = pxcv.views.utils.ViewSortMixin


# Base class for row sorting algorithms.
#  
# Extending classes should provide a sortBy* function for each column that
# needs to be sortable.  The getSortFunc method will transform a Ruby style
# property name into a JavaScript style method name, ie. created_at becomes
# sortByCreatedAt. 
class window.pxcv.views.utils.RowSorterBase
  
  # Extending classes may provide a default for column
  constructor: (column, direction='asc') ->
    _.extend @, ViewSortMixin
    @column    = String column
    @direction = String direction
      
  # Get a sort function for column and direction
  getSortFunc: (column, direction) ->
    @column    = String column    if column?
    @direction = String direction if direction? 
    
    @options = {}
    @options.desc = true if @direction is 'desc'
    
    # get yo' meth
    meth = 'sortBy' + @column.camelize()
    if @[meth]? then @[meth] else @getDefaultSortFunc @column
    
  # Default sort for a column is to sort by string using localCompare
  getDefaultSortFunc: (column) ->
    sort = (a, b) =>
      @propCompare a, b, column, @options
  
  # Universal created_at sort
  sortByCreatedAt: (a, b) =>
    @propCompare a, b, 'created_at', _.extend @options, {date: true}
