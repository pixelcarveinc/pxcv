

# Mixin for sorting views by their model
pxcv.views.utils.ViewSortMixin =

  # Compare a and b views containing models by model prop.
  #
  # Options are:
  #
  # numeric(false)  - sort values as numbers instead of strings
  # date(false)     - convert values to Date and sort numerically
  # desc(false)     - sort descending
  propCompare: (a, b, prop, options={}) ->
    av = a.model.get prop
    bv = b.model.get prop
    [av, bv] = [bv, av] if options.desc is true
      
    
    if options.date is true
      [av, bv] = [new Date(av), new Date(bv)]
      options.numeric = true
       
    if av? and bv?
      if options.numeric is true
        av - bv
      else
        av.localeCompare bv
    else
      @sortUndefined av, bv
        
  # Sort a and b assuming one or both are undefined
  sortUndefined: (a, b) ->
    if a? or b? then (if a? then -1 else 1) else 0  
