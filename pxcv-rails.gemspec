$:.push File.expand_path('../lib', __FILE__)

require 'pxcv-rails/version'

Gem::Specification.new do |gem|
  gem.name        = 'pxcv-rails'
  gem.version     = Pxcv::Rails::VERSION
  gem.authors     = ['Pixelcarve Inc.']
  gem.email       = ['operations@pixelcarve.com']
  gem.homepage    = 'http://pixelcarve.com/'
  gem.summary     = 'Pixelcarve standard library for rails, a shared codebase for Rails projects'
  gem.description = 'Pixelcarve standard library for rails'

  gem.files      = Dir['{app,lib,vendor}/**/*']
  gem.test_files = Dir['{spec,test}/**/*']
  
  gem.add_development_dependency 'rake'
  gem.add_development_dependency 'coffee-rails'
  gem.add_development_dependency 'haml_coffee_assets'
end